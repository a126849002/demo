 public function printCue($id){
            //撈出資料
            $cid = $id;
            $client = Client::find($id);
            $clientlist = Clientlist::find($client->cid);
            $data = $this->getCueData($id);
            $totalData = $this->getTotalData($id);
            $line_height = '10px';
            $users = User::where('id',$client->uid)->get();
            $userName = $users[0]->name;
            //產生ＰＤＦ頁面 供下載或列印
           
              

            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // set font
            $pdf::SetFont('msungstdlight', '', 4);
            $pdf::SetLineStyle(array('width' => 0.5));
            // add a page
            $pdf::AddPage();
            $changeText = "";
            if($client->isChange==1){
                $changeText = "(異動單)";
            }
            $html = '<div style="text-align:center; width:100%;"><h1>MEDIA SCHEDULE-INTERNET'.$changeText.'</h1></div>';
            $html .=  '<div class="panel panel-default">
                        <div class="panel-heading">
                            <img  src="../public/storage/logo0.png" style="width:10px; height:auto; paddin-top:5px">博崍媒體<br>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div style="text-align:left">
                                <label>Client：'. $client->name .'</lable><br>
                                <label>Product：'. $client->product_name .'</label><br>
                                <label>執行期間：'. $client->start_date .' ~ '. $client->end_date .'</label><br>
                                <label>提案時間：'. $client->proposal_date.'</label><br>
                            </div>
                            <table border="1" cellspacing="0"  style="max-width:980px;table-layout:fixed;margin:auto;overflow:auto; display:block; width:100%;border-width:1px">
                                ';
                                //表格標題
                                $html .='<tbody>
                                                 <tr role="row" style="background-color: #a5a5a5; color: black;">
                                                    <td  style="text-align:center;vertical-align: middle;width:30px;line-height:'.$line_height.'">網站</td>
                                                    <td  style="text-align:center;vertical-align: middle;width:30px;line-height:'.$line_height.'">頻道/版位</td>
                                                    <td  style="text-align:center;vertical-align: middle;width:35px;line-height:'.$line_height.'">廣告名稱</td>
                                                    <td  style="text-align:center;vertical-align: middle;width:15px;line-height:'.$line_height.'">R/F</td>
                                                    <td  style="text-align:center;vertical-align: middle;width:70px;line-height:'.$line_height.'">素材規格</td>
                                                    <td  style="text-align:center;vertical-align: middle;width:30px;">預估<br>IMP</td>
                                                    <td  style="text-align:center;vertical-align: middle;width:30px;line-height:'.$line_height.'">類型</td>
                                                    <td  style="text-align:center;vertical-align: middle;width:30px;">預估<br>Clicks/Views</td>
                                                    <td  style="text-align:center;vertical-align: middle;width:25px;">預估<br>CTR/VT</td>
                                                    <td  style="text-align:center;vertical-align: middle;width:50px;" colspan="2">Price</td>
                                                    <td  style="text-align:center;vertical-align: middle;width:30px;">執行期間<br>Period</td>
                                                    <td  style="text-align:center;vertical-align: middle;width:30px;">素材繳交<br>dateline</td>
                                                    <td  style="text-align:center;vertical-align: middle;">媒體預算<br>budget</td>
                                                    <td  style="text-align:center;vertical-align: middle;width:100px;line-height:'.$line_height.'" colspan="2">備註</td>
                                                </tr>';          
                                //遊歷各網站
                                foreach($data as $website){
                                            //每個網站的版位
                                            foreach($website["cues"] as $row){
                                    $html .='   <tr role="row" style="background-color:  white; color: black;">';
                                                    if($row->id == $website["cues"]->get(0)->id) {
                                                        $html .='<td  rowspan="'. count($website["cues"]) .'" style="text-align:center;vertical-align: middle;">'. $row->websiteName .'</td>';
                                                    }
                                                    $html .='<td  align="center" valign="middle">'. $row->page .'</td>
                                                    <td  style="text-align:left;vertical-align: middle;">'. $row->adName .'</td>
                                                    <td  style="text-align:center;vertical-align: middle;">'. $row->R_F .'</td>
                                                    <td  style="text-align:left;vertical-align: middle;">'. str_replace("\n","<br>",$row->material_sd ) .'</td>
                                                    <td  style="text-align:right;vertical-align: middle;">'. number_format($row->IMP) .'</td>
                                                    <td  style="text-align:center;vertical-align: middle;">'. $row->type.'</td>
                                                    <td  style="text-align:right;vertical-align: middle;">'. number_format($row->Clicks_Views) .'</td>
                                                    <td  style="text-align:right;vertical-align: middle;">'. number_format($row->CTR_VT,2) .'%</td>
                                                    <td  style="text-align:center;vertical-align: middle;">'. $row->unit.'</td>
                                                    <td  style="text-align:right;vertical-align: middle;">NT$'. number_format($row->unitValue,2) .'</td>
                                                    <td  style="text-align:center;vertical-align: middle;">'. $row->period .'</td>
                                                    <td  style="text-align:center;vertical-align: middle;">'. $row->dateline .'</td>
                                                    <td  style="text-align:right;vertical-align: middle;">NT$'. number_format($row->budget) .'</td>
                                                    <td  style="text-align:left;vertical-align: middle;" colspan="2">'. str_replace("\n","<br>",$row->remarks ).'</td>
                                                </tr>';
                                            }
                                    //每個網站的sub total
                                    $html .= '  <tr style="background-color: #a5a5a5; color: black;">
                                                    <td  colspan="5" style="text-align:center;vertical-align: middle;">Sub-Total</td>
                                                    <td  style="text-align:right;vertical-align: middle;">'. number_format($website["sumIMP"]) .'</td>
                                                    <td></td>
                                                    <td  style="text-align:right;vertical-align: middle;">'. number_format($website["sumClicks_Views"]) .'</td>
                                                    <td  style="text-align:right;vertical-align: middle;">'. number_format($website["sumCTR_VT"],2) .'%</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td  style="text-align:right;vertical-align: middle;">NT$'. number_format($website["sumBudget"]) .'</td>
                                                    <td colspan="2"></td>
                                                </tr>
                                            </tbody>';
                                }
                                    //總total
                                    $html .='<tbody>
                                                <tr style="background-color: white; color: black;">
                                                    <td  colspan="5" style="text-align:center;vertical-align: middle;">Total</td>
                                                    <td  style="text-align:right;vertical-align: middle;">'. number_format($totalData["totalIMP"]) .'</td>
                                                    <td></td>
                                                    <td  style="text-align:right;vertical-align: middle;">'. number_format($totalData["totalClicks_Views"]) .'</td>
                                                    <td  style="text-align:right;vertical-align: middle;">'. number_format($totalData["totalCTR_VT"],2) .'%</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td  style="text-align:center;vertical-align: middle;">NT$'. number_format($totalData["totalBudget"]).'</td>
                                                    <td colspan="2"></td>
                                                </tr>
                                                <tr style="background-color: white; color: black;">
                                                    <td style="text-align:center;vertical-align: middle;" rowspan="4">備註</td>
                                                    <td class="left" colspan="11" rowspan="4" ></td>
                                                    <td class="left">小計(NET)$</td>
                                                    <td style="text-align:right;vertical-align: middle;">'. number_format($totalData["totalBudget"]).'</td>
                                                </tr>
                                                <tr style="background-color: white; color: black;">
                                                    <td class="left">AC('.($clientlist->tax/100).'%)</td>
                                                    <td style="text-align:right;vertical-align: middle;">'.number_format($totalData["totalBudget"]*($clientlist->tax/100)) .'</td>
                                                </tr>
                                                <tr style="background-color: white; color: black;">
                                                    <td class="left">稅金(5%)$</td>
                                                    <td style="text-align:right;vertical-align: middle;">'.number_format($totalData["totalBudget"]*0.05) .'</td>
                                                </tr>
                                                <tr style="background-color: white; color: black;">
                                                    <td class="left">總計$</td>
                                                    <td style="text-align:right;vertical-align: middle;">'.number_format($totalData["totalBudget"]*(1+0.05+($clientlist->tax/100))).'</td>
                                                </tr>
                                            </tbody>
                                    </table>
                                    <br>
                                    <br>
                                    <div style="display:inline;">CLIENT:</div>
                                    <div style="display:inline;text-align:right;paddin-right:100px;">博崍媒體：'.$userName.'</div>
                                    <br>
                                    <hr>
                                </div> 
                            </div>
                          ';
            

          
            // output the HTML content
            $pdf::writeHTML($html, true, 0, true, 0);
              
            // reset pointer to the last page
            
            //Close and output PDF document
            //$pdf->Output($pdfPath, 'F');//寫入目錄
             $pdf::Output($client->name.'-'.$client->product_name.'.pdf', 'I');

            if($client->checked==true){
                
                return view('cue.printCue' , compact('cid','data','client','totalData','html'));
            }else{

                return view('cue.cue' , compact('cid','data','client','totalData'));
            }

    }