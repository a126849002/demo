import requests
from bs4 import BeautifulSoup

keywordSet = {
    '台北市',
    '新北市',
    '基隆市',
    '宜蘭縣',
    '桃園市',
    '新竹市',
    '新竹縣',
    '苗栗縣',
    '台中市',
    '彰化縣',
    '南投縣',
    '雲林縣',
    '嘉義市',
    '嘉義縣',
    '台南市',
    '高雄市',
    '屏東縣',
    '花蓮縣',
    '台東縣',
    '澎湖縣',
    '金門縣',
    '連江縣',
    '南海諸島'
}
for keyword in keywordSet:
    #設定post參數
    siteload = {
        'strTargetField':'COUNTY',
        'strKeyWords':keyword
    }
    res = requests.post("http://www.ibon.com.tw/retail_inquiry_ajax.aspx",data = siteload)
    soup = BeautifulSoup(res.text, 'html.parser')
    #td很多取index為%3=2的為地址
    t=0
    for i in soup.find_all('td'):
        if t%3==2:
            #將有些只有“地址”的td篩掉
            if i.get_text()!=" 地址":
                #避免有店面太大佔兩個門號 所以只取到第一個門號
                print(i.get_text()[:i.get_text().find("號")+1])
        t = t +1